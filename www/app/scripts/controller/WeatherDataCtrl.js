/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2013 Artevelde University College Ghent
 */

function WeatherDataCtrl($scope, $rootScope, $http) {
    $http.defaults.useXDomain = true;
    //var url = "http://api.openweathermap.org/data/2.5/forecast/daily?q=Gent,be&cnt=10&units=metric&lang=nl&mode=json&callback=JSON_CALLBACK";
    /**
     * Laravel project: https://bitbucket.org/olivierparent/nmdad-iii_laravel_demo
     *
     * voer dit eerst uit op de RESTful server:
     *
     * $ php artisan migrate --seed
     *
     * Je kan enkel testen met het echt IP-adres aan je lokale server, niet met localhost of 127.0.0.1
     * Gebruik bijvoorbeeld ifconfig (Mac OS X) of ipconfig (Windows)
     *
     * @type {string}
     */
    var url = "http://192.168.0.213:8080/NMDAD-III_laravel_demo/public/weather/city/1/day?jsonp=JSON_CALLBACK";
    $http.jsonp(url)
    .success(function(data) {
        $scope.forecast = data;
        $rootScope.city = data[0].city.name;
        console.log($rootScope.city);
        console.log($scope.forecast[0]);
    })
    .error(function() {
        $scope.forecast = [{}];
        console.log("error");
    });

    $scope.week = [];

    for (var i = 0, today = new Date().getDay(), week = ["zondag","maandag","dinsdag","woensdag","donderdag","vrijdag","zaterdag"]; i < 20; i++, today++) {
        if (6 < today) {
            today = 0;
        }
        $scope.week.push(week[today]);
    }
}
