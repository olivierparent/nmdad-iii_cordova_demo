
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                                           *
     *                                                                           *
     *                                                                           *
     *                        aaaAAaaa            HHHHHH                         *
     *                     aaAAAAAAAAAAaa         HHHHHH                         *
     *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
     *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
     *                   aAAAAAa    aAAAAAA                                      *
     *                   AAAAAa      AAAAAA                                      *
     *                   AAAAAa      AAAAAA                                      *
     *                   aAAAAAa     AAAAAA                                      *
     *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
     *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
     *                      aAAAAAAAAAAAAAA       HHHHHH                         *
     *                         aaAAAAAAAAAA       HHHHHH                         *
     *                                                                           *
     *                                                                           *
     *                                                                           *
     *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
     *                                                                           *
     *                                                                           *
     *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
     *                                                                           *
     *                                                                           *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

     Olivier Parent
     Copyright © 2013 Artevelde University College Ghent


New Media Design & Development III - Cordova 3 Demoproject
==========================================================

Installatie-instructies
-----------------------

### Cordova updaten

    $ npm update -g cordova

De versie bepalen

    $ cordova -v

### Cordova project maken

    $ cordova create NMDAD-III_cordova_demo be.arteveldehogeschool.nmdad3 "NMDAD-III"
    $ cd NMDAD-III_cordova_demo
    $ cordova platform add android
    $ cordova plugin add org.apache.cordova.battery-status
    $ cordova plugin add org.apache.cordova.device

### Standaard HelloWorld project verwijderen en "NMDAD-III" downloaden van git

    $ rm -rf www
    $ git init
    $ git remote add origin https://bitbucket.org/olivierparent/nmdad-iii_cordova_demo.git
    $ git fetch
    $ git checkout -t origin/master

### Bower componenten installeren

    $ bower install

### Testen

#### Test met Android Device

    $ cordova run android

#### Of test met Android Virtual Device

    $ cordova emulate android
